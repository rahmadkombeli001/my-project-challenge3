package com.example.challengetiga.bagianfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengetiga.adapternya.KataAdapter
import com.example.challengetiga.databinding.FragmentKataBinding


class KataFragment : Fragment() {

    private var _binding : FragmentKataBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKataBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.title = "Words That Start With ${arguments?.getString("KEY_ALPHABETS")}"

        val aList = arguments?.getStringArrayList("KEY_WORDS")
        val adapter = KataAdapter(aList as ArrayList<String>)

        binding.rvKata.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvKata.adapter = adapter

    }
}