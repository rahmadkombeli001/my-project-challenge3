package com.example.challengetiga.bagianfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengetiga.MainActivity
import com.example.challengetiga.adapternya.AbcdAdapter
import com.example.challengetiga.databinding.FragmentAbcdBinding


class AbcdFragment : Fragment() {
    private var _binding : FragmentAbcdBinding? = null
    private val binding get() = _binding!!
    private val dataSet = MainActivity().DataKi

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAbcdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = AbcdAdapter()
        adapter.submitData(dataSet)

        binding.rvAbcd.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvAbcd.adapter = adapter
    }


}