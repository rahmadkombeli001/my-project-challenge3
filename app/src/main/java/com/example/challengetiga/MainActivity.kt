package com.example.challengetiga

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.challengetiga.bagianfragment.DataKi

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    val DataKi = arrayListOf(
        DataKi('A', arrayListOf("Agent powers", "Agent Jeff Trigger", "Amorphous Shape", "Anne Boonchuy", "Abalone")),
        DataKi('B', arrayListOf("Bill Cipher", "Blind Ivan", "Bats Biker", "Blendin Blandin", "Balon")),
        DataKi('C', arrayListOf("Candi Chiu", "Carla Mccorkle", "Caryn Pines", "Cacing", "Cicak")),
        DataKi('D', arrayListOf("Daun", "Dilan", "Dmasiv", "Durian", "Datang")),
        DataKi('E', arrayListOf("Edit", "Epexol", "Eceng", "Endapan", "Ebi")),
        DataKi('F', arrayListOf("Frisian", "Flag", "Famous", "Fix You", "Favorite")),
        DataKi('G', arrayListOf("Gundam", "Geli", "Gaduh", "Gamon", "Gartic")),
        DataKi('H', arrayListOf("Hijau", "Hitam", "Hawa", "Hampa", "Hutan")),
        DataKi('I', arrayListOf("Indah", "Ikan", "Ijazah", "Izin", "Ideal")),
        DataKi('J', arrayListOf("Jagung", "Jemput", "Jalan", "Jadi", "Jual")),
        DataKi('K', arrayListOf("Kelas", "Kayu", "Kursi", "Kampak", "Kuda")),
        DataKi('L', arrayListOf("Lintas", "Lampu", "Linjur", "Lily", "Law")),
        DataKi('M', arrayListOf("Musim", "Mayur", "Mimpi", "Muda", "Maya")),
        DataKi('N', arrayListOf("Nasi", "Nyanyi", "Nenek", "Nafas", "Nitip")),
        DataKi('O', arrayListOf("Objek", "Orang", "Obat", "Oyo", "Odeng")),
        DataKi('P', arrayListOf("Payung", "Paparazi", "Papa", "Pipi", "Perut")),
        DataKi('Q', arrayListOf("Queker Oats", "Quantum", "Qualified", "Quality", "Queen")),
        DataKi('R', arrayListOf("Raja", "Ratu", "Rapi", "Riskan", "Rambut")),
        DataKi('S', arrayListOf("Satu", "Suka", "Sedih", "Sirna", "Salon")),
        DataKi('T', arrayListOf("Tunggu", "Titip", "Tiyan", "Tutup", "Train")),
        DataKi('U', arrayListOf("Udang", "Ubi", "Ucup", "Uang", "Udah")),
        DataKi('V', arrayListOf("Vallet", "Vendor", "Valid", "Vague", "Vast")),
        DataKi('W', arrayListOf("Wow", "Wonderfull", "Whistle", "Wish", "Water")),
        DataKi('X', arrayListOf("X-Ray", "Xenial", "Xilem", "Xeric", "Xenium")),
        DataKi('Y', arrayListOf("Year", "Yes", "Yap", "Yummy", "Yuppi")),
        DataKi('Z', arrayListOf("Zahra", "Zebra", "Ziggy", "Zagga", "Zizi")),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment

        navController = navHostFragment.navController
        setupActionBarWithNavController(navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }




}